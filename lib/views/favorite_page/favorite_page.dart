import 'package:bec_movie_application/view_models/services/favorite_manage.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class FavoritePage extends ConsumerStatefulWidget {
  const FavoritePage({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _FavoritePageState();
}

class _FavoritePageState extends ConsumerState<FavoritePage> {
  @override
  Widget build(BuildContext context) {
    final listFav = ref.watch(getFavoriteListProvider);
    return listFav.when(
      data: (data) {
        if (data.isEmpty) {
          return const Center(
            child: Text('List is emty'),
          );
        } else {
          return ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, index) => Text(data[index].title!),
          );
        }
      },
      error: (error, stackTrace) => Text(error.toString()),
      loading: () => const Center(child: CircularProgressIndicator()),
    );
  }
}
