import 'package:bec_movie_application/models/movie_model.dart';
import 'package:bec_movie_application/view_models/provider/get_credits_movie_provider.dart';
import 'package:bec_movie_application/view_models/provider/get_detail_movie_provider.dart';
import 'package:bec_movie_application/view_models/provider/get_recommendation_movie_provider.dart';
import 'package:bec_movie_application/view_models/provider/get_similar_movie_provider.dart';
import 'package:bec_movie_application/common/helpers/strings.dart';

import 'package:bec_movie_application/common/helpers/urls.dart';
import 'package:bec_movie_application/common/widgets/horizontal_credit_list_widget.dart';
import 'package:bec_movie_application/common/widgets/horizontal_movie_list_widget.dart';
import 'package:bec_movie_application/common/widgets/text_widget.dart';
import 'package:bec_movie_application/view_models/services/favorite_manage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';

class MovieDetailPage extends ConsumerWidget {
  final int movieId;
  const MovieDetailPage(this.movieId, {super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final FavoritesManager favoritesManager = FavoritesManager();
    final movie = ref.watch(movieDetailsProvider(movieId));
    final recommendationMovie = ref.watch(recommendationMovieProvider(movieId));
    final movieCredits = ref.watch(movieCreditsProvider(movieId));
    final similarMovie = ref.watch(similarMovieProvider(movieId));
    return Scaffold(
      body: movie.when(
        data: (data) {
          return CustomScrollView(
            physics: const BouncingScrollPhysics(),
            slivers: <Widget>[
              SliverAppBar(
                pinned: true,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                    bottom: Radius.circular(5),
                  ),
                ),
                expandedHeight: 280,
                stretch: true,
                flexibleSpace: FlexibleSpaceBar(
                  stretchModes: const [
                    StretchMode.zoomBackground,
                  ],
                  background: CachedNetworkImage(
                    imageUrl: Urls.imagesUrl + data.backdropPath.toString(),
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            bottomLeft: Radius.circular(5),
                            bottomRight: Radius.circular(5)),
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => const Center(
                      child: CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(height: 10),
                    Text(
                      data.title!,
                      textAlign: TextAlign.center,
                      style: const TextStyle(fontSize: 30),
                    ),
                    const SizedBox(height: 10),
                    Text(data.tagline!, textAlign: TextAlign.center),
                    const Divider(endIndent: 10, indent: 10),
                    ElevatedButton.icon(
                        onPressed: () {},
                        icon: const Icon(Icons.play_arrow),
                        label: const Text('Play Trailer')),
                    ElevatedButton.icon(
                        onPressed: () async {
                          final dataMovieToFav = MovieModel(
                            title: data.title,
                            id: data.id,
                            posterPath: data.posterPath,
                          );
                          final listFav = await favoritesManager.getFavorites();
                          if (listFav.contains(dataMovieToFav)) {
                            return;
                          } else {
                            favoritesManager.addFavorite([dataMovieToFav]);
                          }
                        },
                        icon: const Icon(Icons.favorite),
                        label: const Text('Add to favorite list')),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(data.overview!),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        TextWidget.textSubTitle(
                            'Release Date', data.releaseDate!),
                        TextWidget.textSubTitle(
                            Strings.runtime, '${data.runtime} min'),
                        TextWidget.textSubTitle(
                            Strings.voteAverage, data.voteAverage.toString()),
                        TextWidget.textSubTitle(
                            Strings.voteCount, data.voteCount.toString()),
                        TextWidget.textSubTitle(
                          Strings.budget,
                          NumberFormat.currency(
                                  symbol: 'USD ', decimalDigits: 2)
                              .format(data.budget),
                        ),
                        TextWidget.textSubTitle(
                          Strings.revenue,
                          NumberFormat.currency(
                                  symbol: 'USD ', decimalDigits: 2)
                              .format(data.revenue),
                        ),
                        const Divider(endIndent: 10, indent: 10),
                        TextWidget.textTitle(Strings.movieCredits),
                        HorizontalCreditListWidget(movieCredits),
                        const SizedBox(height: 30),
                        TextWidget.textTitle(Strings.similar),
                        HorizontalMovieListWidget(similarMovie),
                        const SizedBox(height: 30),
                        TextWidget.textTitle(Strings.recommendations),
                        HorizontalMovieListWidget(recommendationMovie),
                        const SizedBox(height: 50)
                      ],
                    ),
                  ],
                ),
              ),
            ],
          );
        },
        error: (error, stackTrace) => Text(error.toString()),
        loading: () => const Center(child: CircularProgressIndicator()),
      ),
    );
  }
}
