import 'package:bec_movie_application/view_models/provider/get_searching_movie_provider.dart';
import 'package:bec_movie_application/views/detail_page/movie_detail.dart';

import 'package:bec_movie_application/common/widgets/card_movie_result_widget.dart';

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class _ClearButton extends StatelessWidget {
  const _ClearButton({required this.controller});

  final TextEditingController controller;

  @override
  Widget build(BuildContext context) => IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () => controller.clear(),
      );
}

class SearchPage extends ConsumerStatefulWidget {
  const SearchPage({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _SearchPageState();
}

class _SearchPageState extends ConsumerState<SearchPage> {
  final _controller = TextEditingController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final query = ref.watch(moviesSearchTextProvider);
    final movies = ref.watch(searchingMovieProvider(query));
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              child: Center(
                child: TextField(
                  controller: _controller,
                  decoration: InputDecoration(
                    prefixIcon: const Icon(Icons.search),
                    suffixIcon: _ClearButton(controller: _controller),
                    labelText: 'Find your favorite movie',
                    border: const OutlineInputBorder(),
                  ),
                  onEditingComplete: () {
                    FocusManager.instance.primaryFocus?.unfocus();
                  },
                  onSubmitted: (value) =>
                      FocusManager.instance.primaryFocus?.unfocus(),
                  onChanged: (text) =>
                      ref.read(moviesSearchTextProvider.notifier).state = text,
                ),
              ),
            ),
            const Divider(
              height: 20,
              thickness: 3,
              indent: 20,
              endIndent: 20,
            ),
            movies.when(
              data: (movies) => movies.isEmpty
                  ? const Text('Type something to search')
                  : GridView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      padding: const EdgeInsets.all(20),
                      itemCount: movies.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              crossAxisSpacing: 5,
                              mainAxisSpacing: 5,
                              childAspectRatio: 0.7),
                      itemBuilder: (context, index) => GestureDetector(
                        onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                MovieDetailPage(movies[index].id!),
                          ),
                        ),
                        child: CardMovieResult(movie: movies[index]),
                      ),
                    ),
              loading: () => const CircularProgressIndicator(),
              error: (error, stack) => Text(error.toString()),
            )
          ],
        ),
      ),
    );
  }
}
