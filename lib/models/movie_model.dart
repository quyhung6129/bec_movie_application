class MovieModel {
  int? id;

  String? posterPath;

  String? title;

  String? backdropPath;

  MovieModel({this.id, this.posterPath, this.title, this.backdropPath});

  MovieModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    posterPath = json['poster_path'];
    title = json['title'];
    backdropPath = json['backdrop_path'];
  }

  static Map<String, dynamic> toMap(MovieModel movieModel) => {
        'id': movieModel.id,
        'posterPath': movieModel.posterPath,
        'title': movieModel.title,
        'backdropPath': movieModel.backdropPath,
      };

  Map<String, dynamic> toJson() => {
        'id': id,
        'posterPath': posterPath,
        'title': title,
        'backdropPath': backdropPath,
      };
}
