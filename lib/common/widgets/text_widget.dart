import 'package:flutter/material.dart';

class TextWidget {
  TextWidget._();
  static Widget textTitle(String title) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          title,
          style: const TextStyle(fontSize: 20),
        ),
      );

  static Widget textSubTitle(String title, String sub) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text.rich(
          TextSpan(
            children: [
              TextSpan(text: '$title: '),
              TextSpan(
                text: sub,
              ),
            ],
          ),
        ),
      );
}
