import 'package:bec_movie_application/common/helpers/strings.dart';
import 'package:bec_movie_application/models/movie_credits_model.dart';

import 'package:bec_movie_application/common/helpers/urls.dart';
import 'package:bec_movie_application/common/widgets/shimmer_item_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class HorizontalCreditListWidget extends ConsumerWidget {
  final AsyncValue<List<MovieCreditsModel>> listData;
  const HorizontalCreditListWidget(this.listData, {super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return listData.when(
      data: (data) {
        if (data.isEmpty) {
          return const SizedBox(
            child: Center(
              child: Text(Strings.noCreditsFound),
            ),
          );
        } else {
          return SizedBox(
            height: 200,
            child: ListView.builder(
              shrinkWrap: true,
              itemExtent: 150,
              padding: const EdgeInsets.only(left: 10),
              scrollDirection: Axis.horizontal,
              itemCount: data.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {},
                  child: CachedNetworkImage(
                      imageUrl:
                          Urls.imagesUrl + data[index].profilePath.toString(),
                      imageBuilder: (context, imageProvider) => Card(
                            child: Container(
                              width: 150,
                              height: 200,
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(5),
                                ),
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                      placeholder: (context, url) => const ShimmerItem(),
                      errorWidget: (context, url, error) {
                        if (url == 'https://image.tmdb.org/t/p/originalnull') {
                          return const Card(
                            child: SizedBox(
                              width: 150,
                              child: Icon(Icons.error),
                            ),
                          );
                        } else {
                          return const Card(
                            child: SizedBox(
                              width: 150,
                              child: Icon(Icons.signal_cellular_no_sim),
                            ),
                          );
                        }
                      }),
                );
              },
            ),
          );
        }
      },
      loading: () => SizedBox(
        height: 200,
        child: ListView.builder(
          padding: const EdgeInsets.only(left: 10),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          itemBuilder: (context, index) {
            return const ShimmerItem();
          },
        ),
      ),
      error: (error, stack) => Center(child: Text(error.toString())),
    );
  }
}
