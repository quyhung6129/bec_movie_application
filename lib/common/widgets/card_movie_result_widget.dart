import 'package:bec_movie_application/models/movie_model.dart';
import 'package:bec_movie_application/common/helpers/urls.dart';
import 'package:bec_movie_application/common/widgets/shimmer_item_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CardMovieResult extends StatelessWidget {
  final MovieModel movie;
  const CardMovieResult({super.key, required this.movie});

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: Urls.imagesUrl + movie.posterPath.toString(),
      imageBuilder: (context, imageProvider) => Card(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(5)),
            image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
          ),
        ),
      ),
      placeholder: (context, url) => const ShimmerItem(),
      errorWidget: (context, url, error) =>
          const Card(child: Icon(Icons.error)),
    );
  }
}
