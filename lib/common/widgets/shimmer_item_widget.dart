import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerItem extends StatelessWidget {
  const ShimmerItem({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Colors.black26,
      highlightColor: Colors.grey,
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
      ),
    );
  }
}
