import 'package:bec_movie_application/views/favorite_page/favorite_page.dart';
import 'package:bec_movie_application/views/home_page/home_page.dart';
import 'package:bec_movie_application/views/search_page/search_page.dart';
import 'package:flutter/material.dart';

const List<NavigationDestination> appBarDestinations = [
  NavigationDestination(
    tooltip: '',
    icon: Icon(Icons.home_outlined),
    label: 'Home',
    selectedIcon: Icon(Icons.home),
  ),
  NavigationDestination(
    tooltip: '',
    icon: Icon(Icons.search_outlined),
    label: 'Search',
    selectedIcon: Icon(Icons.search),
  ),
  NavigationDestination(
    tooltip: '',
    icon: Icon(Icons.favorite_outline),
    label: 'Favorite',
    selectedIcon: Icon(Icons.favorite),
  ),
];

List pageWidget = [
  const HomePage(),
  const SearchPage(),
  const FavoritePage(),
];
