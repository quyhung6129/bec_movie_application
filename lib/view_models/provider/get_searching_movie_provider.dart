import 'package:bec_movie_application/models/movie_model.dart';
import 'package:bec_movie_application/view_models/services/tmdb_config_api.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final searchingMovieProvider = FutureProvider.family<List<MovieModel>, String>(
  (ref, movieName) async {
    final services = ref.watch(servicesProvider);
    return services.getSearchingMovieByName(movieName);
  },
);

// StateProvider that can be watched by other widgets to obtain the
// current search query.
final moviesSearchTextProvider = StateProvider<String>((ref) {
  return '';
});
