import 'dart:convert';

import 'package:bec_movie_application/models/movie_model.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

final favoriteManProvider = Provider((ref) {
  return FavoritesManager();
});

final getFavoriteListProvider = FutureProvider<List<MovieModel>>((ref) async {
  final listData = ref.watch(favoriteManProvider).getFavorites();

  return listData;
});

class FavoritesManager {
  static const String _favoritesKey = 'favorites';

  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Future<List<MovieModel>> getFavorites() async {
    final SharedPreferences prefs = await _prefs;
    final String? objectsString = prefs.getString(_favoritesKey);
    if (objectsString != null) {
      final List<dynamic> objectList = json.decode(objectsString);
      final List<MovieModel> myObjects = objectList
          .map((jsonObject) => MovieModel.fromJson(jsonObject))
          .toList();
      return myObjects;
    }
    return []; // Return an empty list if no objects are found
  }

  Future<void> addFavorite(List<MovieModel> myObjects) async {
    final SharedPreferences prefs = await _prefs;
    final List<dynamic> objectList =
        myObjects.map((object) => object.toJson()).toList();
    final String objectsString = json.encode(objectList);
    await prefs.setString(_favoritesKey, objectsString);
  }

  Future<void> removeFavorite(MovieModel item) async {
    final List<MovieModel> objects = await getFavorites();
    objects.remove(item);
    await addFavorite(objects);
  }

  // Future<bool> isFavorite(String item) async {
  //   final List<String> favorites = await getFavorites();
  //   return favorites.contains(item);
  // }
}
